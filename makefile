BDIR=bin

SOURCE_ROM=Kirby64.z64
TARGET_ROM=Kirby64_mod.z64

F3DGEN_DIR=f3dgen
K64GEO_DIR=f3dgen_linkers
INSERTBIN_DIR=insertbin

F3DGEN=$(F3DGEN_DIR)/bin/f3dgen
K64GEO=$(K64GEO_DIR)/bin/k64geo
INSERTBIN=$(INSERTBIN_DIR)/bin/insertbin

##########

.phony: tools
tools:
	make -C $(F3DGEN_DIR)
	make -C $(K64GEO_DIR)
	make -C $(INSERTBIN_DIR)

$(F3DGEN):
	make -C $(F3DGEN_DIR)

$(K64GEO):
	make -C $(K64GEO_DIR)

$(INSERTBIN):
	make -C $(INSERTBIN_DIR)

##########

SAMPLE_DAE_DIR=sample_dae
SAMPLE_DAE=$(SAMPLE_DAE_DIR)/test2.dae

TARGET_GAR=$(BDIR)/test.gar
TARGET_GAR_MANIFEST=$(TARGET_GAR)/MANIFEST
TARGET_GEOBIN=$(BDIR)/test_geo.bin

.phony: test
test: $(TARGET_ROM)

$(TARGET_GAR): $(SAMPLE_DAE) $(F3DGEN) | $(BDIR)
	$(F3DGEN) -vscale 70 -o $@ $(SAMPLE_DAE)

$(TARGET_GEOBIN): $(K64GEO) $(TARGET_GAR) | $(BDIR)
	$(K64GEO) $(TARGET_GAR) $(TARGET_GEOBIN)

$(TARGET_ROM): $(SOURCE_ROM) $(TARGET_GEOBIN) $(INSERTBIN)
	$(INSERTBIN) 0x012B4420 $(SOURCE_ROM) $(TARGET_GEOBIN) $(TARGET_ROM)

.phony: cleantest
cleantest:
	rm -rf $(BDIR)
	rm -rf $(TARGET_ROM)

##########

$(BDIR):
	mkdir $(BDIR)

##########

.phony: clean
clean:
	rm -rf $(BDIR)
	make clean -C $(F3DGEN_DIR)
	make clean -C $(K64GEO_DIR)
	make clean -C $(INSERTBIN_DIR)