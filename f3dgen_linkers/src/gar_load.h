#ifndef GAR_LOAD_H
#define GAR_LOAD_H

#include <stdio.h>

typedef enum
{
    GAR_OBJECT_DEFAULT,
    GAR_OBJECT_IMAGE,
    GAR_OBJECT_VERTS,
    GAR_OBJECT_UCODE
} gar_object_type_t;

typedef enum
{
    GAR_RELOC_DEFAULT,
    GAR_RELOC_SETTIMG,
    GAR_RELOC_VTX
} gar_reloc_type_t;

typedef struct
{
    unsigned int offset;
    gar_reloc_type_t type;
    const char* symbol;
    unsigned int symbol_offset;
} gar_object_reloc_t;

typedef struct
{
    unsigned int address;
    int b_address_assigned;
    const char* symbol;
    const char* path;
    unsigned int data_size;
    unsigned char* data;
    unsigned int num_relocs;
    unsigned int num_reloc_slots;
    gar_object_reloc_t* relocs;
} gar_object_t;

struct gar_node_t;
typedef struct gar_node_t
{
    const char* symbol;
    const char* ucode_symbol;
    float matrix[16];
    unsigned int num_child_nodes;
    unsigned int num_child_node_slots;
    struct gar_node_t* child_nodes;
    struct gar_node_t* parent_node;
} gar_node_t;

typedef struct
{
    char* text;
    char* text_tok;
    unsigned int pos;
    unsigned int length;
} gar_parse_ctx_t;

typedef struct
{
    unsigned int length;
    unsigned int num_slots;
    gar_object_t* array;
} gar_object_set_t;

typedef struct
{
    const char* path;
    gar_object_set_t default_objects;
    gar_object_set_t image_objects;
    gar_object_set_t verts_objects;
    gar_object_set_t ucode_objects;
    gar_node_t root_node;
    gar_node_t* cur_parent_node;
    gar_parse_ctx_t pctx;
    gar_object_t* cur_object;
} gar_t;

int gar_load(gar_t* gar, const char* path);
int gar_unload(gar_t* gar);

#endif // GAR_LOAD_H

#define GAR_LOAD_IMPL // remove me

#ifdef GAR_LOAD_IMPL

#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#define gar_bswap32(n) ( \
	(((unsigned int)(n) & 0xFF000000) >> 24) | \
	(((unsigned int)(n) & 0x00FF0000) >>  8) | \
	(((unsigned int)(n) & 0x0000FF00) <<  8) | \
	(((unsigned int)(n) & 0x000000FF) << 24)   \
)

int gar_node_init(gar_node_t* gar_node, const char* symbol, const char* ucode_symbol, const char* matrix_string);
int gar_node_free(gar_node_t* gar_node);
gar_node_t* gar_node_add_child(gar_node_t* gar_node, gar_node_t gar_node_child);
//int gar_node_set_parent(gar_node_t* gar_node, gar_node_t* gar_node_parent);

int gar_parser_is_eof(gar_t* gar);
int gar_parser_inc_pos(gar_t* gar);
char gar_parser_curc(gar_t* gar);
int gar_parser_skip_comment(gar_t* gar);
int gar_parser_seek_token(gar_t* gar);
const char* gar_parser_extract_command_token(gar_t* gar);
const char* gar_parser_extract_symbol_token(gar_t* gar);
const char* gar_parser_extract_string_token(gar_t* gar);
int gar_parser_expect_char(gar_t* gar, char c);

gar_object_set_t* gar_get_object_set(gar_t* gar, gar_object_type_t type);
gar_object_t* gar_get_object(gar_t* gar, gar_object_type_t type, int index);
int gar_num_objects(gar_t* gar, gar_object_type_t type);

int gar_object_init(gar_object_t* gar_object, const char* symbol, const char* path, const char* root_path);
int gar_object_free(gar_object_t* gar_object);
int gar_add_object(gar_t* gar, gar_object_t gar_object, gar_object_type_t type);

int gar_object_add_reloc(gar_object_t* gar_object, gar_object_reloc_t reloc);

int gar_parser_process_object_command(gar_t* gar, gar_object_type_t type);
int gar_parser_process_reloc_command(gar_t* gar);
int gar_parser_run(gar_t* gar);
int gar_bind_objects(gar_t* gar, gar_object_type_t type, unsigned int base_address);
int gar_copy_objects(gar_t* gar, gar_object_type_t type, unsigned char* dest);
gar_object_t* gar_get_object_by_symbol(gar_t* gar, gar_object_type_t type, const char* symbol);
unsigned int gar_get_symbol_address(gar_t* gar, const char* symbol);
int gar_link_all(gar_t* gar);
unsigned int gar_sizeof_objects(gar_t* gar, gar_object_type_t type);
unsigned int gar_num_relocs_objects(gar_t* gar, gar_object_type_t type);
//int gar_load(gar_t* gar, const char* path);

int gar_count_nodes(gar_t* gar);
int gar_node_num_child_nodes_r(gar_node_t* gar_node, int* count);

int gar_parser_is_eof(gar_t* gar)
{
    return (gar->pctx.pos >= gar->pctx.length);
}

int gar_parser_inc_pos(gar_t* gar)
{
    if(!gar_parser_is_eof(gar))
    {
        gar->pctx.pos++;
        return 1;
    }
    return 0;
}

char gar_parser_curc(gar_t* gar)
{
    return gar->pctx.text[gar->pctx.pos];
}

int gar_parser_skip_comment(gar_t* gar)
{
    char curc;

    while(!gar_parser_is_eof(gar))
    {
        gar_parser_inc_pos(gar);
        curc = gar_parser_curc(gar);
        if(curc == '\n')
        {
            break;
        }
    }
    return 1;
}

int gar_parser_seek_token(gar_t* gar)
{
    char curc;

    while(!gar_parser_is_eof(gar))
    {
        curc = gar_parser_curc(gar);

        if(isspace(curc))
        {
            gar_parser_inc_pos(gar);
        }
        else if(curc == '#')
        {
            gar_parser_skip_comment(gar);
        }
        else
        {
            return 1;
        }
    }

    return 0; // EOF
}

const char* gar_parser_extract_command_token(gar_t* gar)
{
    const char* token;
    char curc;

    if(!gar_parser_seek_token(gar))
    {
        return NULL;
    }

    token = &gar->pctx.text_tok[gar->pctx.pos];

    while(!gar_parser_is_eof(gar))
    {
        curc = gar_parser_curc(gar);
        if(isspace(curc))
        {
            gar->pctx.text_tok[gar->pctx.pos] = '\0';
            break;
        }
        gar_parser_inc_pos(gar);
    }

    return token;
}

const char* gar_parser_extract_symbol_token(gar_t* gar)
{
    const char* token;
    char curc;

    if(!gar_parser_seek_token(gar))
    {
        return NULL;
    }

    token = &gar->pctx.text_tok[gar->pctx.pos];

    while(!gar_parser_is_eof(gar))
    {
        curc = gar_parser_curc(gar);
        if(isspace(curc) || curc == ',') // todo rather check for end of valid symbol characters
        {
            gar->pctx.text_tok[gar->pctx.pos] = '\0';
            break;
        }
        gar_parser_inc_pos(gar);
    }

    return token;
}

const char* gar_parser_extract_string_token(gar_t* gar)
{
    const char* token;
    char curc;

    if(!gar_parser_seek_token(gar))
    {
        return NULL;
    }

    curc = gar_parser_curc(gar);

    if(curc != '"')
    {
        return NULL;
    }

    gar_parser_inc_pos(gar);

    if(gar_parser_is_eof(gar))
    {
        return NULL;
    }
    
    token = &gar->pctx.text_tok[gar->pctx.pos];

    while(!gar_parser_is_eof(gar))
    {
        curc = gar_parser_curc(gar);
        if(curc == '"')
        {
            gar->pctx.text_tok[gar->pctx.pos] = '\0';
            gar_parser_inc_pos(gar);
            return token;
        }
        gar_parser_inc_pos(gar);
    }

    return NULL;
}

int gar_parser_expect_char(gar_t* gar, char c)
{
    //char curc;

    if(!gar_parser_seek_token(gar))
    {
        return 0;
    }

    if(gar_parser_curc(gar) != c)
    {
        return 0;
    }

    gar_parser_inc_pos(gar);
    return 1;
}

gar_object_set_t* gar_get_object_set(gar_t* gar, gar_object_type_t type)
{
    switch(type)
    {
    case GAR_OBJECT_DEFAULT:
        return &gar->default_objects;
    case GAR_OBJECT_IMAGE:
        return &gar->image_objects;
    case GAR_OBJECT_VERTS:
        return &gar->verts_objects;
    case GAR_OBJECT_UCODE:
        return &gar->ucode_objects;
    }
    return NULL;
}

gar_object_t* gar_get_object(gar_t* gar, gar_object_type_t type, int index)
{
    gar_object_set_t* gar_object_set = gar_get_object_set(gar, type);

    if(index < gar_object_set->length)
    {
        return &gar_object_set->array[index];
    }
    return NULL;
}

int gar_num_objects(gar_t* gar, gar_object_type_t type)
{
    gar_object_set_t* gar_object_set = gar_get_object_set(gar, type);
    return gar_object_set->length;
}

int gar_object_init(gar_object_t* gar_object, const char* symbol, const char* path, const char* root_path)
{
    FILE* fp;
    char full_path[256];

    memset(gar_object, 0, sizeof(gar_object_t));

    //gar_object->type = type;
    gar_object->symbol = symbol;
    gar_object->path = path;

    snprintf(full_path, 256, "%s/%s", root_path, path);

    fp = fopen(full_path, "rb");

    if(fp == NULL)
    {
        return 0;
    }

    fseek(fp, 0, SEEK_END);

    gar_object->data_size = ftell(fp);
    rewind(fp);

    gar_object->data = (unsigned char*)malloc(gar_object->data_size);
    fread(gar_object->data, 1, gar_object->data_size, fp);

    return 1;
}

int gar_object_free(gar_object_t* gar_object)
{
    free(gar_object->data);
    free(gar_object->relocs);
    return 1;
}

int gar_add_object(gar_t* gar, gar_object_t gar_object, gar_object_type_t type)
{
    gar_object_set_t* gar_object_set = gar_get_object_set(gar, type);

    if(gar_object_set->num_slots == 0)
    {
        gar_object_set->num_slots = 1;
        gar_object_set->array = (gar_object_t*)malloc(sizeof(gar_object_t));
    }

    if(gar_object_set->length >= gar_object_set->num_slots)
    {
        gar_object_set->num_slots *= 2;
        gar_object_set->array = (gar_object_t*)realloc(gar_object_set->array, gar_object_set->num_slots * sizeof(gar_object_t));
    }

    gar_object_set->array[gar_object_set->length] = gar_object;
    gar->cur_object = &gar_object_set->array[gar_object_set->length];
    return gar_object_set->length++;
}

int gar_object_add_reloc(gar_object_t* gar_object, gar_object_reloc_t reloc)
{
    if(gar_object->num_reloc_slots == 0)
    {
        gar_object->num_reloc_slots = 1;
        gar_object->relocs = (gar_object_reloc_t*)malloc(sizeof(gar_object_reloc_t));
    }

    if(gar_object->num_relocs >= gar_object->num_reloc_slots)
    {
        gar_object->num_reloc_slots *= 2;
        gar_object->relocs = (gar_object_reloc_t*)realloc(gar_object->relocs, gar_object->num_reloc_slots * sizeof(gar_object_reloc_t));
    }

    gar_object->relocs[gar_object->num_relocs] = reloc;
    return gar_object->num_relocs++;
}

int gar_parser_process_object_command(gar_t* gar, gar_object_type_t type)
{
    gar_object_t gar_object;

    const char* symbol;
    const char* path;

    symbol = gar_parser_extract_symbol_token(gar);

    if(!gar_parser_expect_char(gar, ','))
    {
        return 0;
    }
    
    path = gar_parser_extract_string_token(gar);

    gar_object_init(&gar_object, symbol, path, gar->path);
    gar_add_object(gar, gar_object, type);

    printf("symbol: %s, path: %s, size: %d\n", symbol, path, gar_object.data_size);

    return 1;
}

int gar_parser_process_reloc_command(gar_t* gar)
{
    gar_object_reloc_t reloc;

    const char* sz_offset, *sz_type, *sz_symbol, *sz_symbol_offset = NULL;
    //unsigned int offset;

    sz_offset = gar_parser_extract_symbol_token(gar);
    reloc.offset = strtol(sz_offset, NULL, 0);

    if(!gar_parser_expect_char(gar, ','))
    {
        return 0;
    }

    sz_type = gar_parser_extract_symbol_token(gar);

    if(!gar_parser_expect_char(gar, ','))
    {
        return 0;
    }

    sz_symbol = gar_parser_extract_symbol_token(gar);
    reloc.symbol = sz_symbol;

    if(gar_parser_expect_char(gar, ','))
    {
        sz_symbol_offset = gar_parser_extract_symbol_token(gar);
        reloc.symbol_offset = strtol(sz_symbol_offset, NULL, 0);
        //return 0;
    }
    else
    {
        reloc.symbol_offset = 0;
    }

    if(strcmp(sz_type, "G_VTX") == 0)
    {
        reloc.type = GAR_RELOC_VTX;
    }
    else if(strcmp(sz_type, "G_SETTIMG") == 0)
    {
        reloc.type = GAR_RELOC_SETTIMG;
    }
    else
    {
        reloc.type = GAR_RELOC_DEFAULT;
    }

    gar_object_add_reloc(gar->cur_object, reloc);

    printf("%08x %d %s %08X\n", reloc.offset, reloc.type, reloc.symbol, reloc.symbol_offset);

    return 1;
}

int gar_parser_run(gar_t* gar)
{
    const char* command;

    while((command = gar_parser_extract_command_token(gar)))
    {
        if(strcmp(command, "image") == 0)
        {
            gar_parser_process_object_command(gar, GAR_OBJECT_IMAGE);
            continue;
        }

        if(strcmp(command, "verts") == 0)
        {
            gar_parser_process_object_command(gar, GAR_OBJECT_VERTS);
            continue;
        }

        if(strcmp(command, "ucode") == 0)
        {
            gar_parser_process_object_command(gar, GAR_OBJECT_UCODE);
            continue;
        }

        if(strcmp(command, "reloc") == 0)
        {
            gar_parser_process_reloc_command(gar);
            continue;
        }

        // todo b_node_context

        if(strcmp(command, "node") == 0)
        {
            gar_node_t gar_node;
            const char* node_symbol, *ucode_symbol, *matrix_string;

            node_symbol = gar_parser_extract_symbol_token(gar);
            gar_parser_expect_char(gar, ',');
            ucode_symbol = gar_parser_extract_symbol_token(gar);
            gar_parser_expect_char(gar, ',');
            matrix_string = gar_parser_extract_string_token(gar);

            printf("node %s\ngeom %s\nmat4 %s\n", node_symbol, ucode_symbol, matrix_string);

            gar_node_init(&gar_node, node_symbol, ucode_symbol, matrix_string);
            gar_node_t* gar_node_ptr = gar_node_add_child(gar->cur_parent_node, gar_node);
            gar->cur_parent_node = gar_node_ptr;
            continue;
        }

        if(strcmp(command, "endnode") == 0)
        {
            gar->cur_parent_node = gar->cur_parent_node->parent_node;
            printf("endnode\n");
            continue;
        }

        break;
    }
    return 1;
}

int gar_bind_objects(gar_t* gar, gar_object_type_t type, unsigned int base_address)
{
    gar_object_set_t* set = gar_get_object_set(gar, type);

    for(int i = 0; i < set->length; i++)
    {
        gar_object_t* gar_object = &set->array[i];

        gar_object->address = base_address;
        gar_object->b_address_assigned = 1;

        base_address += gar_object->data_size;
    }

    return base_address;
}

int gar_copy_objects(gar_t* gar, gar_object_type_t type, unsigned char* dest)
{
    gar_object_set_t* set = gar_get_object_set(gar, type);

    for(int i = 0; i < set->length; i++)
    {
        gar_object_t* gar_object = &set->array[i];

        memcpy(dest, gar_object->data, gar_object->data_size);

        dest += gar_object->data_size;
    }

    return 1;
}

gar_object_t* gar_get_object_by_symbol(gar_t* gar, gar_object_type_t type, const char* symbol)
{
    gar_object_set_t* set = gar_get_object_set(gar, type);

    for(int i = 0; i < set->length; i++)
    {
        gar_object_t* gar_object = &set->array[i];

        if(strcmp(gar_object->symbol, symbol) == 0)
        {
            return gar_object;
        }
    }

    return NULL;
}

unsigned int gar_get_symbol_address(gar_t* gar, const char* symbol)
{
    gar_object_t* gar_object;

    gar_object = gar_get_object_by_symbol(gar, GAR_OBJECT_VERTS, symbol);

    if(gar_object != NULL)
    {
        if(gar_object->b_address_assigned)
        {
            return gar_object->address;
        }
    }

    gar_object = gar_get_object_by_symbol(gar, GAR_OBJECT_IMAGE, symbol);

    if(gar_object != NULL)
    {
        if(gar_object->b_address_assigned)
        {
            return gar_object->address;
        }
    }

    gar_object = gar_get_object_by_symbol(gar, GAR_OBJECT_UCODE, symbol);

    if(gar_object != NULL)
    {
        if(gar_object->b_address_assigned)
        {
            return gar_object->address;
        }
    }

    return 0;
}

int gar_link_all(gar_t* gar)
{
    // only ucode for now
    gar_object_set_t* set_ucode = gar_get_object_set(gar, GAR_OBJECT_UCODE);
    
    for(int i = 0; i < set_ucode->length; i++)
    {
        gar_object_t* gar_object = &set_ucode->array[i];

        for(int i = 0; i < gar_object->num_relocs; i++)
        {
            gar_object_reloc_t* reloc = &gar_object->relocs[i];

            unsigned int symbol_address = gar_get_symbol_address(gar, reloc->symbol); // todo need proper null check

            printf("%08X -> %08X\n", reloc->offset, symbol_address + reloc->symbol_offset);

            *(unsigned int*)(&gar_object->data[reloc->offset]) = gar_bswap32(symbol_address + reloc->symbol_offset);
        }
    }

    return 1;
}

unsigned int gar_sizeof_objects(gar_t* gar, gar_object_type_t type)
{
    gar_object_set_t* object_set = gar_get_object_set(gar, type);

    unsigned int total_size = 0;

    for(int i = 0; i < object_set->length; i++)
    {
        total_size += object_set->array[i].data_size;
    }

    return total_size;
}

unsigned int gar_num_relocs_objects(gar_t* gar, gar_object_type_t type)
{
    gar_object_set_t* object_set = gar_get_object_set(gar, type);

    unsigned int total_relocs = 0;

    for(int i = 0; i < object_set->length; i++)
    {
        total_relocs += object_set->array[i].num_relocs;
    }

    return total_relocs;
}

int gar_load(gar_t* gar, const char* path)
{
    FILE* fp;
    char manifest_path[256];

    memset(gar, 0, sizeof(gar_t));

    gar_node_init(&gar->root_node, ".root", NULL, NULL);
    gar->cur_parent_node = &gar->root_node;

    gar->path = path;

    sprintf(manifest_path, "%s/MANIFEST", path);

    fp = fopen(manifest_path, "rb");

    if(fp == NULL)
    {
        return 0;
    }

    fseek(fp, 0, SEEK_END);

    gar->pctx.length = ftell(fp);
    gar->pctx.text = (char*)malloc(gar->pctx.length + 1);
    gar->pctx.text_tok = (char*)malloc(gar->pctx.length + 1);

    rewind(fp);

    fread((char*)gar->pctx.text, 1, gar->pctx.length, fp);
    memcpy(gar->pctx.text_tok, gar->pctx.text, gar->pctx.length);

    gar->pctx.text[gar->pctx.length] = '\0';
    gar->pctx.text_tok[gar->pctx.length] = '\0';

    fclose(fp);

    gar_parser_run(gar);
    return 1;
}

////////////////////////////////

int gar_node_init(gar_node_t* gar_node, const char* symbol, const char* ucode_symbol, const char* matrix_string)
{
    memset(gar_node, 0, sizeof(gar_node_t));

    gar_node->symbol = symbol;
    gar_node->ucode_symbol = ucode_symbol;

    if(matrix_string != NULL)
    {
        char* matrix_string_tok = (char*)matrix_string;

        for(int i = 0; i < 16; i++)
        {
            if(matrix_string_tok == NULL)
            {
                printf("bad matrix string");
                break;
            }
            gar_node->matrix[i] = strtof(matrix_string_tok, &matrix_string_tok);
        }
    }
    return 1;
}

int gar_node_free(gar_node_t* gar_node)
{
    if(gar_node->child_nodes != NULL)
    {
        for(int i = 0; i < gar_node->num_child_nodes; i++)
        {
            gar_node_free(&gar_node->child_nodes[i]);
        }
        free(gar_node->child_nodes);
    }
    return 1;
}

gar_node_t* gar_node_add_child(gar_node_t* gar_node, gar_node_t gar_node_child)
{
    if(gar_node->num_child_node_slots == 0)
    {
        gar_node->child_nodes = (gar_node_t*)malloc(sizeof(gar_node_t));
        gar_node->num_child_node_slots = 1;
    }

    if(gar_node->num_child_nodes >= gar_node->num_child_node_slots)
    {
        gar_node->num_child_node_slots *= 2;
        gar_node->child_nodes = (gar_node_t*)realloc(gar_node->child_nodes, sizeof(gar_node_t) * gar_node->num_child_node_slots);
    }

    gar_node_child.parent_node = gar_node;

    gar_node->child_nodes[gar_node->num_child_nodes] = gar_node_child;
    return &gar_node->child_nodes[gar_node->num_child_nodes++]; // return pointer to child node after it's added
}

int gar_count_nodes(gar_t* gar)
{
    int count = 0;
    gar_node_num_child_nodes_r(&gar->root_node, &count);
    return count;
}

int gar_node_num_child_nodes_r(gar_node_t* gar_node, int* count)
{
    *count += gar_node->num_child_nodes;
    for(int i = 0; i < gar_node->num_child_nodes; i++)
    {
        gar_node_num_child_nodes_r(&gar_node->child_nodes[i], count);
    }
    return 1;
}

#endif // GAR_LOAD_IMPL