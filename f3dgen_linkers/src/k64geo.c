#include <stdint.h>
#include <math.h>
#include <string.h>

#include "gar_load.h"

typedef struct
{
    uint32_t layout_sec_addr;
    uint32_t tex_scroll_addr;
    uint32_t layout_mode;
    uint32_t image_refs_addr;
    uint32_t verts_refs_addr;
    int num_things;
    uint32_t things_addr;
    int unknown; // number of nodes in layout sec before 0x12 command?
} kirby64_geom_header_t;

typedef struct
{
    uint32_t x, y, z; // raw BE floats
} kirby64_geom_vec3_t;

typedef struct
{
    uint32_t command;
    uint32_t geom_addr; // mode 0x18 = group addr, mode 0x17 = dlist addr
    kirby64_geom_vec3_t translation;
    kirby64_geom_vec3_t rotation;
    kirby64_geom_vec3_t scale;
} kirby64_geom_layout_entry_t;

typedef struct
{
    uint32_t command;
    uint32_t address;
} kirby64_geom_group_command_t;

#define K64_GEOM_LAYOUT_MODE_18 0x18

uint32_t align(uint32_t v, uint32_t a)
{
    if((v % 16) == 0) return v;
    return v + (a - (v % a));
}

#define _sq(n) (n*n)
#define M_PI 3.14159265358979323846

typedef struct
{
    float a, b, c, d,
          e, f, g, h,
          i, j, k, l,
          m, n, o, p;
} gar_mat4_fields_t;

typedef struct
{
    float x, y, z;
} gar_vec3f_t;

typedef struct
{
    uint32_t x, y, z;
} gar_vec3u_t;

typedef union
{
    gar_vec3f_t f;
    gar_vec3u_t u;
} gar_vec3_mixed_t;

void mat4_decompose(float* matrix_in, gar_vec3f_t* trans, gar_vec3f_t* scale, gar_vec3f_t* rot)
{
    gar_mat4_fields_t m4;
    memcpy(&m4, matrix_in, sizeof(float) * 16);

    trans->x = m4.d; // test
    trans->y = m4.h;
    trans->z = m4.l;

    scale->x = sqrt(_sq(m4.a) + _sq(m4.e) + _sq(m4.i));
    scale->y = sqrt(_sq(m4.b) + _sq(m4.f) + _sq(m4.j));
    scale->z = sqrt(_sq(m4.c) + _sq(m4.g) + _sq(m4.k));

    m4.a /= scale->x;
    m4.b /= scale->y;
    m4.c /= scale->z;

    m4.e /= scale->x;
    m4.f /= scale->y;
    m4.g /= scale->z;

    m4.i /= scale->x;
    m4.j /= scale->y;
    m4.k /= scale->z;

    rot->x = atan2(m4.j, m4.k);
    rot->y = atan2(-m4.i, sqrt(_sq(m4.j) + _sq(m4.k)));
    rot->z = atan2(m4.e, m4.a);
}

int init_groups_r(uint8_t* block, gar_t* gar, gar_node_t* gar_node, uint32_t* groups_block_offset, uint32_t* layout_block_offset)
{
    kirby64_geom_group_command_t* group_commands = (kirby64_geom_group_command_t*)(&block[*groups_block_offset]);
    kirby64_geom_layout_entry_t* layout_command = (kirby64_geom_layout_entry_t*)(&block[*layout_block_offset]);

    if(gar_node->ucode_symbol != NULL)
    {
        uint32_t ucode_addr = gar_get_symbol_address(gar, gar_node->ucode_symbol);

        printf("adding group commands %s %08X\n", gar_node->ucode_symbol, ucode_addr);

        // push group commands

        group_commands[0].command = 0;
        group_commands[0].address = gar_bswap32(ucode_addr);

        group_commands[1].command = gar_bswap32(0x00000004); // terminate
        group_commands[1].address = 0;

        // push layout command

        gar_vec3_mixed_t trans, scale, rot;
        mat4_decompose(gar_node->matrix, &trans.f, &scale.f, &rot.f);

        layout_command->command = gar_bswap32(0x00000001); // todo recursion level?
        layout_command->geom_addr = gar_bswap32(0x04000000 + *groups_block_offset);

        layout_command->translation.x = gar_bswap32(*(uint32_t*)(char*)&trans.u.x);
        layout_command->translation.y = gar_bswap32(*(uint32_t*)(char*)&trans.u.y);
        layout_command->translation.z = gar_bswap32(*(uint32_t*)(char*)&trans.u.z);

        layout_command->rotation.x = gar_bswap32(*(uint32_t*)(char*)&rot.u.x);
        layout_command->rotation.y = gar_bswap32(*(uint32_t*)(char*)&rot.u.y);
        layout_command->rotation.z = gar_bswap32(*(uint32_t*)(char*)&rot.u.z);

        layout_command->scale.x = gar_bswap32(*(uint32_t*)(char*)&scale.u.x);
        layout_command->scale.y = gar_bswap32(*(uint32_t*)(char*)&scale.u.y);
        layout_command->scale.z = gar_bswap32(*(uint32_t*)(char*)&scale.u.z);

        *layout_block_offset += sizeof(kirby64_geom_layout_entry_t);
        *groups_block_offset += sizeof(kirby64_geom_group_command_t) * 2;  
    }
    else
    {
        // root node
        layout_command->command = 0;
        layout_command->geom_addr = 0;

        layout_command->translation.x = 0;
        layout_command->translation.y = 0;
        layout_command->translation.z = 0;

        layout_command->rotation.x = 0;
        layout_command->rotation.y = 0;
        layout_command->rotation.z = 0;

        layout_command->scale.x = gar_bswap32(0x3F800000);
        layout_command->scale.y = gar_bswap32(0x3F800000);
        layout_command->scale.z = gar_bswap32(0x3F800000);

        *layout_block_offset += sizeof(kirby64_geom_layout_entry_t);
    }

    for(int i = 0; i < gar_node->num_child_nodes; i++)
    {
        init_groups_r(block, gar, &gar_node->child_nodes[i], groups_block_offset, layout_block_offset);
    }

    return 1;
}

int init_groups(uint8_t* block, gar_t* gar, uint32_t groups_block_offset, uint32_t layout_block_offset)
{
    int num_nodes = gar_count_nodes(gar);
    uint32_t last_layout_command_offset = layout_block_offset + sizeof(kirby64_geom_layout_entry_t) * (num_nodes + 1);

    //printf("adding group commands\n");
    init_groups_r(block, gar, &gar->root_node, &groups_block_offset, &layout_block_offset);

    printf("%08X ~~~~~~~~~~\n", last_layout_command_offset);

    kirby64_geom_layout_entry_t* last_layout_command = (kirby64_geom_layout_entry_t*)&block[last_layout_command_offset];
    last_layout_command->command = gar_bswap32(0x00000012);
    last_layout_command->geom_addr = gar_bswap32(0x00000000); // 0x80000000 if more data follows?

    return 1;
}

int kirby64_geom_header_init(kirby64_geom_header_t* header, uint32_t layout_sec_addr, uint32_t tex_scroll_addr, uint32_t layout_mode, uint32_t img_refs_addr,
    uint32_t vtx_refs_addr, int num_things, uint32_t things_addr, uint32_t unknown)
{
    header->layout_sec_addr = gar_bswap32(layout_sec_addr);
    header->tex_scroll_addr = 0;
    header->layout_mode = gar_bswap32(layout_mode);
    header->image_refs_addr = gar_bswap32(img_refs_addr);
    header->verts_refs_addr = gar_bswap32(vtx_refs_addr);
    header->num_things = 0;
    header->things_addr = things_addr;
    header->unknown = gar_bswap32(unknown);

    return 1;
}

int main(int argc, const char* argv[])
{
    const char* input_path, *output_path;

    if(argc < 3)
    {
        printf("k64 gar linker\n");
        printf("usage: k64geo <input_path> <output_path>\n\n");
    }

    input_path = argv[1];
    output_path = argv[2];

    printf("[k64geo] input: %s, output: %s\n\n", input_path, output_path);

    gar_t gar;
    gar_load(&gar, input_path);

    int num_nodes = gar_count_nodes(&gar);

    printf("\n\nlinker test\n\n");

    gar_node_t* test_node = &gar.root_node.child_nodes[0];
    printf("%s\n", test_node->symbol);

    gar_vec3f_t trans, scale, rot;
    mat4_decompose(test_node->matrix, &trans, &scale, &rot);

    printf("num_nodes: %d\n", num_nodes);

    // mode 0x18 block size calculation
    //
    // header = 0x20
    // sizeof verts, ucode, images
    // img rels       = (num * 4) + 4 for null terminator
    // vert rels      = (num * 4) + 4 for null terminator
    // dlist groups   = num nodes * 0x10
    // layout section = 0x2C for empty root node + (0x2C * num nodes) + 0x2C terminator

    uint32_t header_sec_size = 0x20;
    uint32_t block_addr = 0x04000000;

    uint32_t verts_end_addr = gar_bind_objects(&gar, GAR_OBJECT_VERTS, block_addr + header_sec_size);
    uint32_t ucode_end_addr = gar_bind_objects(&gar, GAR_OBJECT_UCODE, verts_end_addr);

    ucode_end_addr = align(ucode_end_addr, 16);

    //uint32_t image_end_addr =
    gar_bind_objects(&gar, GAR_OBJECT_IMAGE, ucode_end_addr);

    gar_link_all(&gar);

    uint32_t verts_sec_size = gar_sizeof_objects(&gar, GAR_OBJECT_VERTS);
    uint32_t ucode_sec_size = gar_sizeof_objects(&gar, GAR_OBJECT_UCODE);
    uint32_t image_sec_size = gar_sizeof_objects(&gar, GAR_OBJECT_IMAGE);

    ucode_sec_size = align(ucode_sec_size, 16);

    //uint32_t num_ucode_relocs = gar_num_relocs_objects(&gar, GAR_OBJECT_UCODE);
    
    uint32_t verts_sec_block_offset = 0 + header_sec_size;
    uint32_t ucode_sec_block_offset = verts_sec_block_offset + verts_sec_size;
    uint32_t image_sec_block_offset = ucode_sec_block_offset + ucode_sec_size;

    uint32_t image_refs_block_offset = image_sec_block_offset + image_sec_size;
    uint32_t image_refs_addr = block_addr + image_refs_block_offset;
    uint32_t image_refs_sec_size = 4; // temporary

    uint32_t vert_refs_block_offset = image_refs_block_offset + image_refs_sec_size;
    uint32_t vert_refs_addr = block_addr + vert_refs_block_offset;
    uint32_t vert_refs_sec_size = 4; // temporary

    uint32_t ucode_groups_block_offset = vert_refs_block_offset + image_refs_sec_size;
    //uint32_t ucode_groups_addr = block_addr + ucode_groups_block_offset;
    uint32_t ucode_groups_sec_size = num_nodes * 0x10; // one pointer command (8 bytes) + end command (8 bytes)

    uint32_t layout_sec_block_offset = ucode_groups_block_offset + ucode_groups_sec_size;
    uint32_t layout_sec_addr = block_addr + layout_sec_block_offset;
    uint32_t layout_sec_size = (num_nodes * sizeof(kirby64_geom_layout_entry_t)) + (2 * sizeof(kirby64_geom_layout_entry_t));

    uint32_t block_size =
        header_sec_size +
        verts_sec_size +
        ucode_sec_size +
        image_sec_size +
        image_refs_sec_size +
        vert_refs_sec_size +
        ucode_groups_sec_size +
        layout_sec_size;

    block_size = align(block_size, 16);

    ///////////

    uint8_t* block = malloc(block_size);
    memset(block, 0, block_size);

    kirby64_geom_header_t* header = (kirby64_geom_header_t*)block;

    init_groups(block, &gar, ucode_groups_block_offset, layout_sec_block_offset);

    kirby64_geom_header_init(header, layout_sec_addr, 0, K64_GEOM_LAYOUT_MODE_18, image_refs_addr, vert_refs_addr, 0, 0, num_nodes + 1);

    printf("block_size %08X\n", block_size);

    gar_copy_objects(&gar, GAR_OBJECT_VERTS, &block[verts_sec_block_offset]);
    gar_copy_objects(&gar, GAR_OBJECT_UCODE, &block[ucode_sec_block_offset]);
    gar_copy_objects(&gar, GAR_OBJECT_IMAGE, &block[image_sec_block_offset]);

    FILE* fp = fopen(output_path, "wb");
    fwrite(block, 1, block_size, fp);
    fclose(fp);

    free(block);

    //printf("num_images %d, num_ucodes %d, num_verts %d\n", num_images, num_ucodes, num_vertss);

    //gar_unload(&gar);

    return EXIT_SUCCESS;
}