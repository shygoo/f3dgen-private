#include <stdlib.h>

#include "bswap.h"

void bswap16_buffer(void* buf, size_t size)
{
    char* bufc = (char*)buf;
    for(size_t i = 0; i < size; i += sizeof(uint16_t))
    {
        *(uint16_t*)&bufc[i] = bswap16(*(uint16_t*)&bufc[i]);
    }
}

void bswap32_buffer(void* buf, size_t size)
{
    char* bufc = (char*)buf;
    for(size_t i = 0; i < size; i += sizeof(uint32_t))
    {
        *(uint32_t*)&bufc[i] = bswap32(*(uint32_t*)&bufc[i]);
    }
}