#ifndef DAE_H
#define DAE_H

#include "vecprim.h"
#include "xml.h"
#include "img.h"

#define DAE_NO_OFFSET (-1)

// <library_images>

typedef struct
{
	const char* name;
	const char* id;
	const char* init_from;
	img_t img;
} dae_image_t;

VECTOR_TYPEDEF(dae_image_t, dae_image_vec_t, dae_image_vec_)

// <library_effects>

typedef struct
{
	float r, g, b, a;
} dae_col4f_t;

#define DAE_COLOR_EMISSION 1
#define DAE_COLOR_AMBIENT  2
#define DAE_COLOR_DIFFUSE  4
#define DAE_COLOR_SPECULAR 8

typedef struct
{
	const char* id;
	// phong
	unsigned color_flags;
	dae_col4f_t emission_color;
	dae_col4f_t ambient_color;
	dae_col4f_t diffuse_color;
	dae_col4f_t specular_color;
	const char* diffuse_texture;
	float shininess;
	float index_of_refraction;
} dae_effect_t;

VECTOR_TYPEDEF(dae_effect_t, dae_effect_vec_t, dae_effect_vec_)

// <library_materials>

typedef struct
{
	const char* id;
	const char* name;
	const char* effect_id;
} dae_material_t;

VECTOR_TYPEDEF(dae_material_t, dae_material_vec_t, dae_material_vec_)

// <library_geometries>

typedef struct
{
	const char* material_id;
	float_vec_t vertices, normals, colors, texcoords;
	int         vertices_offset, normals_offset, colors_offset, texcoords_offset;
	int         num_inputs;
	int_vec_t   points;
	int num_points;
	int num_faces;
} dae_polylist_t;

VECTOR_TYPEDEF(dae_polylist_t, dae_polylist_vec_t, dae_polylist_vec_)

typedef struct
{
	const char* name;
	const char* id;
	dae_polylist_vec_t polylists;
} dae_geometry_t;

VECTOR_TYPEDEF(dae_geometry_t, dae_geometry_vec_t, dae_geometry_vec_)

// <library_visual_scenes>
// just the first <visual_scene> in the library, geometry instance nodes

struct dae_scene_node_t;
VECTOR_TYPEDEF(struct dae_scene_node_t, dae_scene_node_vec_t, dae_scene_node_vec_)

typedef struct
{
    float m[4][4];
} dae_mat4f_t;

typedef struct dae_scene_node_t
{
	const char* id;
	const char* geometry_id;
	dae_mat4f_t matrix;
	dae_scene_node_vec_t child_nodes;
} dae_scene_node_t;

/////////

typedef struct
{
	const char* root_path;
	xml_t xml;
	dae_image_vec_t images;
	dae_geometry_vec_t geometries;
	dae_effect_vec_t effects;
	dae_material_vec_t materials;
	dae_scene_node_t root_node;
} dae_t;

typedef struct
{
    float x, y, z;
} vec3f_t;

typedef struct
{
	float r, g, b;
} dae_color_t;

typedef struct
{
	float u;
	float v;
} dae_uv_t;

int dae_load(dae_t* dae, const char* path, const char* root_path);
int dae_unload(dae_t* dae);

int dae_init_images(dae_t* dae);
int dae_free_images(dae_t* dae);
int dae_init_geometries(dae_t* dae);
int dae_free_geometries(dae_t* dae);
int dae_init_effects(dae_t* dae);
int dae_free_effects(dae_t* dae);
int dae_init_materials(dae_t* dae);
int dae_free_materials(dae_t* dae);

int dae_init_scene(dae_t* dae);
int dae_free_scene(dae_t* dae);

dae_material_t* dae_get_material(dae_t* dae, const char* material_id);
dae_effect_t*   dae_get_effect(dae_t* dae, const char* effect_id);

int dae_report(dae_t* dae); // debug

int dae_scene_node_init(dae_scene_node_t* dae_scene_node, const char* id, const char* geometry_id, const char* matrix_string);
int dae_scene_node_free(dae_scene_node_t* dae_scene_node);

int dae_image_init(dae_image_t* dae_image, const char* id, const char* name, const char* init_from, const char* root_path);
int dae_image_free(dae_image_t* dae_image);

int dae_geometry_init(dae_geometry_t* dae_geometry, const char* id, const char* name);
int dae_geometry_free(dae_geometry_t* dae_geometry);

int dae_polylist_init(dae_polylist_t* dae_polylist, const char* material_id);
int dae_polylist_free(dae_polylist_t* dae_polylist);

int dae_polylist_get_vertex_vec3(dae_polylist_t* dae_polylist, int point_index, vec3f_t* vertex, double vscale);
int dae_polylist_get_normal_vec3(dae_polylist_t* dae_polylist, int point_index, vec3f_t* normal);
int dae_polylist_get_color_vec3(dae_polylist_t* dae_polylist, int point_index, vec3f_t* color);
int dae_polylist_get_uv(dae_polylist_t* dae_polylist, int point_index, dae_uv_t* dae_uv);

int dae_polylist_get_face_vertices(dae_polylist_t* dae_polylist, int face_idx, vec3f_t* vertices, double vscale);
int dae_polylist_get_face_normals(dae_polylist_t* dae_polylist, int face_idx, vec3f_t* normals);
int dae_polylist_get_face_colors(dae_polylist_t* dae_polylist, int face_idx, vec3f_t* colors);
int dae_polylist_get_face_uvs(dae_polylist_t* dae_polylist, int face_idx, dae_uv_t* dae_uvs);

int float_vec_push_from_string(float_vec_t* fvec, const char* str);
int int_vec_push_from_string(int_vec_t* ivec, const char* str);

int dae_mat4_init_from_string(dae_mat4f_t* m, const char* str);
void dae_mat4_swap_yz(dae_mat4f_t* m);
void dae_mat4_mult(dae_mat4f_t* out, dae_mat4f_t* in1, dae_mat4f_t* in2);

int vec3f_transform(vec3f_t* v, dae_mat4f_t* m);

int dae_col4f_init_from_string(dae_col4f_t* c, const char* str);

int float_vec_print(float_vec_t* fvec, int width, int height); // debug
int int_vec_print(int_vec_t* ivec, int width, int height); // debug
//void mat4f_print(mat4f_t* m); // debug

#endif // DAE_H
