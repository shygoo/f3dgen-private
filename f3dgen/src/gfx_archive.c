#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "gfx_archive.h"

#include "vecdef.h"
#include "vecprim.h"
#include "vertex.h"
#include "dae.h"
#include "dlist.h"
#include "img.h"

VECTOR_IMPLDEF(dlist_rel_t, dlist_rel_vec_t, dlist_rel_vec_)
VECTOR_IMPLDEF(dlist_object_t, dlist_object_vec_t, dlist_object_vec_)
VECTOR_IMPLDEF(gfx_archive_img_t, gfx_archive_img_vec_t, gar_img_vec_)
VECTOR_IMPLDEF(struct gfx_archive_scene_node_t, gfx_archive_scene_node_vec_t, gfx_archive_scene_node_vec_)

int dlist_object_init(dlist_object_t* dlist_object, const char* name, ucode_t ucode)
{
    dlist_object->name = name;
    dlist_init(&dlist_object->dlist, ucode);
    dlist_rel_vec_init(&dlist_object->relocations);
    vertex_list_init(&dlist_object->vertices);
    return 1;
}

int dlist_object_free(dlist_object_t* dlist_object)
{
    dlist_free(&dlist_object->dlist);
    dlist_rel_vec_free(&dlist_object->relocations);
    vertex_list_free(&dlist_object->vertices);
    return 1;
}

int gar_init(gfx_archive_t* gar, ucode_t ucode, uint32_t vbase, double vscale)
{
    gar->ucode = ucode;
    gar->vbase = vbase;
    gar->vscale = vscale;
    //vertex_list_init(&gar->vertices);
    gar_scene_node_init(&gar->root_node, ".root", NULL);
    dlist_object_vec_init(&gar->dlist_objects);
    gar_img_vec_init(&gar->images);
    return 1;
}

int gar_free(gfx_archive_t* gar)
{
    //vertex_list_free(&gar->vertices);
    dlist_object_vec_iterate(&gar->dlist_objects, dlist_object_free);
    dlist_object_vec_free(&gar->dlist_objects);
    // todo need to actually free each image
    gar_img_vec_free(&gar->images);
    gar_scene_node_free(&gar->root_node);
    return 1;
}

int gar_scene_node_init(gfx_archive_scene_node_t* node, const char* id, const char* dlist_name)
{
    node->id = id;
    node->dlist_name = dlist_name;
    gfx_archive_scene_node_vec_init(&node->child_nodes);
    return 1;
}

int gar_scene_node_free(gfx_archive_scene_node_t* node)
{
    for(int i = 0; i < node->child_nodes.length; i++)
    {
        gar_scene_node_free(&node->child_nodes.array[i]);
    }
    gfx_archive_scene_node_vec_free(&node->child_nodes);
    return 1;
}

gfx_archive_img_t* gar_get_img(gfx_archive_t* gar, const char* img_name)
{
    for(int i = 0; i < gar->images.length; i++)
    {
        gfx_archive_img_t* gar_img = &gar->images.array[i];

        if(strcmp(gar_img->name, img_name) == 0)
        {
            return gar_img;
        }
    }
    return NULL;
}

///////////////////////////////////////////

VECTOR_IMPLDEF(tri_t, tri_vector_t, tri_vector_)

int gbuf_init(gbuf_t* gbuf, int max_vertices)
{
    tri_vector_init(&gbuf->tris);
    gbuf->max_vertices = max_vertices;
    gbuf->vidx = 0;
    //gbuf->full = 0;
    return 1;
}

int gbuf_free(gbuf_t* gbuf)
{
    tri_vector_free(&gbuf->tris);
    return 1;
}

int gbuf_clear(gbuf_t* gbuf)
{
    gbuf->vidx = 0;
    //gbuf->full = 0;
    tri_vector_clear(&gbuf->tris);
    return 1;
}

int gbuf_get_vertex_index(gbuf_t* gbuf, vertex_t* vertex)
{
    for(int vidx = 0; vidx < gbuf->vidx; vidx++)
    {
        if(memcmp(&gbuf->vbuf[vidx], vertex, sizeof(vertex_t)) == 0)
        {
            return vidx;
        }
    }
    return -1;
}

int gbuf_has_vertex(gbuf_t* gbuf, vertex_t* vertex)
{
    return gbuf_get_vertex_index(gbuf, vertex) != -1;
}

int gbuf_push_unique_vertex(gbuf_t* gbuf, vertex_t vertex)
{
    int existing_vidx = gbuf_get_vertex_index(gbuf, &vertex);
    
    if(existing_vidx != -1)
    {
        return existing_vidx;
    }

    if(gbuf->vidx < gbuf->max_vertices)
    {
        // push vertex and return its index
        gbuf->vbuf[gbuf->vidx] = vertex;
        return gbuf->vidx++;
    }

    // buffer is full
    return -1;
}

int gbuf_push_tri(gbuf_t* gbuf, int v1, int v2, int v3)
{
    tri_t tri;
    tri.v1 = v1;
    tri.v2 = v2;
    tri.v3 = v3;
    return tri_vector_push(&gbuf->tris, tri);
}

void gbuf_flush(gbuf_t* gbuf, dlist_object_t* dlist_object, uint32_t vertex_base_address)
{
	dlist_t* dlist = &dlist_object->dlist;
    dlist_rel_vec_t* relocations = &dlist_object->relocations;
    vertex_list_t* vlist = &dlist_object->vertices;

    int vertex_list_offset = (vlist->length * sizeof(vertex_t));

    // push vertices to the vertex list
    for(int i = 0; i < gbuf->vidx; i++)
    {
        vertex_t vertex = gbuf->vbuf[i];
        vertex_list_push(vlist, vertex);
    }
    
    // push g_vtx command
    int dlist_index = dlist_push_vtx(dlist, 0, gbuf->vidx, 0) - 1;

    dlist_rel_t rel;
    rel.type = DLIST_REL_VTX;
    rel.dlist_index = dlist_index;
    rel.symbol = NULL; // will default to (dlist_name)_vertices
    rel.symbol_offset = vertex_list_offset;
    dlist_rel_vec_push(relocations, rel);

    // push tri commands
    for(int i = 0; i < gbuf->tris.length; i += 2)
    {
        tri_t tri_a = gbuf->tris.array[i];

        if(i + 1 >= gbuf->tris.length)
        {
            dlist_push_tri1(dlist, tri_a.v1, tri_a.v2, tri_a.v3);
            break;
        }
        
        tri_t tri_b = gbuf->tris.array[i + 1];
        dlist_push_tri2(dlist, tri_a.v1, tri_a.v2, tri_a.v3, tri_b.v1, tri_b.v2, tri_b.v3);
    }

    // kirby does this after a group of tri commands
    dlist_push_pipesync(dlist);

    gbuf_clear(gbuf);
}

int gbuf_is_empty(gbuf_t* gbuf)
{
    return (gbuf->vidx == 0);
}

int gbuf_has_room3(gbuf_t* gbuf, vertex_t* v0, vertex_t* v1, vertex_t* v2)
{
    // calculate space required in the vertex buffer for new vertices
    int have_v0 = gbuf_has_vertex(gbuf, v0);
    int have_v1 = gbuf_has_vertex(gbuf, v1);
    int have_v2 = gbuf_has_vertex(gbuf, v2);
    int num_new_verts = 3 - (have_v0 + have_v1 + have_v2);

    return ((gbuf->vidx + num_new_verts) <= gbuf->max_vertices);
}
