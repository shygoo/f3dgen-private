#include "gfx_archive_dae.h"
#include <string.h>
#include <math.h>

int gar_import_dae(gfx_archive_t* gar, dae_t* dae)
{
    // note: for now dlist_object names point to strings in the dae's xml token buffer
    //  don't unload dae until gfx archive is freed

    gbuf_t gbuf; // vertices/faces conversion context
    int vbuf_size;

    switch(gar->ucode)
    {
    case UCODE_F3DEX2:
    default:
        vbuf_size = 32;
        break;
    }

    // add images

    for(int i = 0; i < dae->images.length; i++)
    {
        dae_image_t* dae_image = &dae->images.array[i];
        gar_add_dae_image(gar, dae_image);
    }

    printf("\n");

    // add geometries (dlists, vertices)
    gbuf_init(&gbuf, vbuf_size);

    for(int i = 0; i < dae->geometries.length; i++)
    {
        dae_geometry_t* geometry = &dae->geometries.array[i];
        gar_add_dae_geometry(gar, dae, geometry, &gbuf);
    }

    gbuf_free(&gbuf);

    // todo make recursive
    gar_add_dae_scene_node(&gar->root_node, &dae->root_node, gar->vscale);

    printf("\n");

    return 1;
}

int gar_add_dae_scene_node(gfx_archive_scene_node_t* gar_scene_parent_node, dae_scene_node_t* dae_scene_node, double scale)
{
    for(int i = 0; i < dae_scene_node->child_nodes.length; i++)
    {
        dae_scene_node_t* dae_scene_child_node = &dae_scene_node->child_nodes.array[i];

        gfx_archive_scene_node_t gar_scene_child_node;
        gar_scene_node_init(&gar_scene_child_node, dae_scene_child_node->id, dae_scene_child_node->geometry_id);
        memcpy(&gar_scene_child_node.matrix, &dae_scene_child_node->matrix, sizeof(dae_mat4f_t));

        // multiply translation vector by vscale
        gar_scene_child_node.matrix[0+3] *= scale;
        gar_scene_child_node.matrix[4+3] *= scale;
        gar_scene_child_node.matrix[8+3] *= scale;

        int idx = gfx_archive_scene_node_vec_push(&gar_scene_parent_node->child_nodes, gar_scene_child_node) - 1;
        gfx_archive_scene_node_t* gar_scene_child_node_ptr = &gar_scene_parent_node->child_nodes.array[idx];

        gar_add_dae_scene_node(gar_scene_child_node_ptr, dae_scene_child_node, scale);
    }

    return 1;
}

int gar_add_dae_image(gfx_archive_t* gar, dae_image_t* dae_image)
{
    img_t* img = &dae_image->img;
    img_ci_t img_ci;
    gfx_archive_img_t gar_img;

    gar_img.name = dae_image->id;

    printf("[gar_dae] converting image '%s'\n", dae_image->id);

    int bgray = img_is_grayscale(img);
    const char* sz_bgray = (bgray ? "true" : "false");

    img_ci_init(&img_ci, img); // create temporary color-indexed version

    printf("[gar_dae]   dimensions: %dx%d, grayscale: %s, num colors: %d\n", img->width, img->height, sz_bgray, img_ci.num_colors);

    // todo decide which n64 image type to use based on dimensions, grayscale, num colors
    // for now just use rgba5551 for everything
    if(img->num_pixels * sizeof(img_pixel5551_t) > 4096)
    {
        printf("[gar_dae]   ERROR: texture size exceeds TMEM maximum\n");
    }
    else
    {
        printf("[gar_dae]   using rgba5551\n");
        gar_img.type = GAR_IMG_RGBA5551;
        img_rgba5551_init(&gar_img.gimg.img_rgba5551, img);
    }

    gar_img_vec_push(&gar->images, gar_img);

    img_ci_free(&img_ci);

    return 1;
}

static void _vertex_set_with_dae_vecs(vertex_t* v, vec3f_t position, vec3f_t color_or_normal, dae_uv_t uv, int img_w, int img_h)
{
    v->x = (int16_t)position.x;
    v->y = (int16_t)position.y;
    v->z = (int16_t)position.z;

    v->_unused = 0;

    v->color.r = (uint8_t)(0xFF * color_or_normal.x);
    v->color.g = (uint8_t)(0xFF * color_or_normal.y);
    v->color.b = (uint8_t)(0xFF * color_or_normal.z);
    v->color.a = 0xFF; // :(

    // TODO accurate fixed point 10.5 conversion

    float texel_u = (img_w - 1) * uv.u;
    float texel_v = (img_h - 1) * (1.0 - uv.v); // V is inverted?
    
    printf("u: %f, v: %f\n", texel_u, texel_v);

    v->u = ((int16_t)round(texel_u)) << 5;
    v->v = ((int16_t)round(texel_v)) << 5;
}

int gar_add_dae_geometry(gfx_archive_t* gar, dae_t* dae, dae_geometry_t* geometry, gbuf_t* gbuf)
{
    dlist_object_t dlist_object;
    dlist_t* dlist = &dlist_object.dlist;
    dlist_rel_vec_t* relocations = &dlist_object.relocations;

    printf("[gar_dae] converting geometry '%s'\n", geometry->id);

    dlist_object_init(&dlist_object, geometry->id, gar->ucode);

    dlist_push_pipesync(dlist);
    dlist_push_tilesync(dlist);

    gbuf_clear(gbuf);

    for(int i = 0; i < geometry->polylists.length; i++)
    {
        printf("[gar_dae]   processing polylist %d\n", i);

        dae_polylist_t* dae_polylist = &geometry->polylists.array[i];
        dae_material_t* dae_material = NULL;
        dae_effect_t* dae_effect = NULL;

        gfx_archive_img_t* gar_img = NULL;

        // push material update commands

        if(dae_polylist->material_id != NULL)
        {
            dae_material = dae_get_material(dae, dae_polylist->material_id);
            printf("[gar_dae]     material: %s\n", dae_polylist->material_id);

            if(dae_material != NULL)
            {
                dae_effect = dae_get_effect(dae, dae_material->effect_id);
                printf("[gar_dae]     effect: %s\n", dae_material->effect_id);
            }
        }
        else
        {
            printf("[gar_dae]     no material\n");
        }

        if(dae_effect != NULL)
        {
            if(dae_effect->diffuse_texture == NULL)
            {
                printf("[gar_dae]     no texture\n");
                // todo pick normal shading or color shading
                dlist_push_geometrymode(dlist, G_ALL & ~G_LIGHTING, G_SHADE | G_SHADING_SMOOTH | G_ZBUFFER);
                dlist_push_setcombine_raw(dlist, 0xFFFFFF, 0xFFFE793C);
                //dlist_push_geometrymode(dlist, G_ALL, G_LIGHTING | G_CULL_BACK);
                //dlist_push_setcombine_raw(dlist, 0x327e64, 0xfffff7fb); // incl primcolor
                dae_col4f_t difcol = dae_effect->diffuse_color;
                uint32_t primcolor = 0;
                primcolor |= (uint8_t)(difcol.r * 255) << 24;
                primcolor |= (uint8_t)(difcol.g * 255) << 16;
                primcolor |= (uint8_t)(difcol.b * 255) << 8;
                primcolor |= (uint8_t)(difcol.a * 255);
                dlist_push_setprimcolor(dlist, primcolor);
                dlist_push_texture(dlist, 0, 0, 0, 0, TEX_OFF); // needed?
            }
            else
            {
                int f3d_im_fmt, f3d_im_siz;
                int dlist_settimg_index;
                int loadblock_dxt, loadblock_num_texels;
                int texcoord_lrs, texcoord_lrt; // fxp 10.2
                int img_w, img_h;

                printf("[gar_dae]     diffuse_texture: %s\n", dae_effect->diffuse_texture);
                // push settimg and linker relocation entry
                
                gar_img = gar_get_img(gar, dae_effect->diffuse_texture);

                switch(gar_img->type)
                {
                    default:
                    case GAR_IMG_RGBA5551:
                        f3d_im_fmt = G_IM_FMT_RGBA;
                        f3d_im_siz = G_IM_SIZ_16b;
                        break;
                }

                // todo fix all dumb image struct definitions
                img_w = gar_img->gimg.img_rgba5551.width;
                img_h = gar_img->gimg.img_rgba5551.height;
                
                // settilesize coords fxp10.2 = (dimension - 1) << 2
                texcoord_lrs = (img_w - 1) << 2;
                texcoord_lrt = (img_h - 1) << 2;

                // 32 * 16 = 0x200 // num bits per row (width * bpp)
                // 0x200 / 64 = 8  // num 64 bit words per row
                // 1 / 8 = 0.00100000000b = 0x100 (fxp 1.11) // dxt field
                loadblock_dxt = 0x100; // TODO temporary
                loadblock_num_texels = img_w * img_h;

                dlist_push_command(dlist, (dlist_command_t){0x00000000, 0xDEADBEEF}); // debug marker
                // TODO geomode
                dlist_push_setcombine_raw(dlist, 0x127E24, 0xFFFFF3F9); // FC (tx with vtx shade)
                //dlist_push_settile(dlist, G_IM_FMT_CI, G_IM_SIZ_16b, 0, 0, TILE_7, 0, 0, 5, 0, 0, 5, 0);  // F5500000 07014050

                // todo 2^mask = num texels to show
                dlist_push_settile(dlist, f3d_im_fmt, f3d_im_siz, (img_w / 8) * 2, 0, TILE_7, 0,
                    G_TX_NOMIRROR | G_TX_WRAP, 5, 0,
                    G_TX_NOMIRROR | G_TX_WRAP, 5, 0);
                
                dlist_push_texture(dlist, 0xFFFF, 0xFFFF, 1, TILE_7, TEX_ON); // D7 kirby uses an extra mipmap level?
                
                dlist_push_settilesize(dlist, TILE_7, 0, 0, texcoord_lrs, texcoord_lrt);
                
                dlist_settimg_index = dlist_push_settimg(dlist, f3d_im_fmt, f3d_im_siz, 1, 0x00000000) - 1; // FD
                dlist_push_loadsync(dlist); // E6
                
                dlist_push_loadblock(dlist, TILE_7, 0, 0, loadblock_num_texels, loadblock_dxt); // F3
                
                dlist_push_pipesync(dlist); // E7
                // TODO settile
                dlist_push_othermode_h(dlist, G_MDSFT_TEXTLUT, 2, G_TT_NONE << G_MDSFT_TEXTLUT); // disable palette
                dlist_push_othermode_h(dlist, G_MDSFT_TEXTFILT, 2, G_TF_POINT << G_MDSFT_TEXTFILT); // disable filtering

                dlist_rel_t rel;
                rel.type = DLIST_REL_SETTIMG;
                rel.dlist_index = dlist_settimg_index;
                rel.symbol = dae_effect->diffuse_texture;
                rel.symbol_offset = 0;
                dlist_rel_vec_push(relocations, rel);
                printf("[gar_dae]     pushed relocation G_SETTIMG %08X -> '%s'\n", dlist_settimg_index*8+4, rel.symbol);
            }
        }

        // push vertex and triangle commands

        for(int i = 0; i < dae_polylist->num_faces; i++)
        {
            vertex_t v0, v1, v2;

            vec3f_t face_colors[3] = {0};
            vec3f_t face_positions[3] = {0};
            vec3f_t face_normals[3] = {0};
            dae_uv_t face_uvs[3] = {0};

            dae_polylist_get_face_vertices(dae_polylist, i, face_positions, gar->vscale);

            // todo prioritize colors over normals

            if(dae_polylist->texcoords_offset != DAE_NO_OFFSET)
            {
                dae_polylist_get_face_uvs(dae_polylist, i, face_uvs);
            }

            if(dae_polylist->normals_offset != DAE_NO_OFFSET)
            {
                dae_polylist_get_face_normals(dae_polylist, i, face_normals);
            }

            if(dae_polylist->colors_offset != DAE_NO_OFFSET)
            {
                dae_polylist_get_face_colors(dae_polylist, i, face_colors);
            }

            int img_width = 0, img_height = 0;

            if(gar_img != NULL)
            {
                // todo fix dumb structs
                img_width = gar_img->gimg.img_rgba5551.width;
                img_height = gar_img->gimg.img_rgba5551.height;
            }

            _vertex_set_with_dae_vecs(&v0, face_positions[0], face_colors[0], face_uvs[0], img_width, img_height);
            _vertex_set_with_dae_vecs(&v1, face_positions[1], face_colors[1], face_uvs[1], img_width, img_height);
            _vertex_set_with_dae_vecs(&v2, face_positions[2], face_colors[2], face_uvs[2], img_width, img_height);

            if(!gbuf_has_room3(gbuf, &v0, &v1, &v2))
            {
                // no more room in the vbuf for this triangle
                // flush commands to dlist, vertex copies to vlist
                gbuf_flush(gbuf, &dlist_object, gar->vbase);
            }

            int vidx0 = gbuf_push_unique_vertex(gbuf, v0);
            int vidx1 = gbuf_push_unique_vertex(gbuf, v1);
            int vidx2 = gbuf_push_unique_vertex(gbuf, v2);
            gbuf_push_tri(gbuf, vidx0, vidx1, vidx2);
        }

        if(!gbuf_is_empty(gbuf))
        {
            // flush remainder
            gbuf_flush(gbuf, &dlist_object, gar->vbase);
        }
    }

    dlist_push_geometrymode(dlist, G_ALL, G_LIGHTING); // kirby seems to do this
    dlist_push_enddl(dlist);

    dlist_object_vec_push(&gar->dlist_objects, dlist_object);

    // add the dlist to the graphics archive
    //dlist_vec_push(&gar->dlists, dlist);
    //cstr_vec_push(&gar->dlist_names, geometry->id);

    return 1;
}
