#ifndef GFX_ARCHIVE_H
#define GFX_ARCHIVE_H

#include "vecdef.h"
#include "vecprim.h"
#include "vertex.h"
#include "dae.h"
#include "dlist.h"
#include "img.h"

// Linkable dlist object

typedef enum
{
    DLIST_REL_VTX,
    DLIST_REL_SETTIMG,
} dlist_rel_type_t;

typedef struct
{
    dlist_rel_type_t type;
    int dlist_index;
    const char* symbol;
    uint32_t symbol_offset;
} dlist_rel_t;

VECTOR_TYPEDEF(dlist_rel_t, dlist_rel_vec_t, dlist_rel_vec_)

typedef struct
{
    const char* name;
    dlist_t dlist;
    dlist_rel_vec_t relocations;
    vertex_list_t vertices;
} dlist_object_t; // linkable dlist object

VECTOR_TYPEDEF(dlist_object_t, dlist_object_vec_t, dlist_object_vec_)

// Scene geometry instance nodes

struct gfx_archive_scene_node_t;
VECTOR_TYPEDEF(struct gfx_archive_scene_node_t, gfx_archive_scene_node_vec_t, gfx_archive_scene_node_vec_)

typedef struct gfx_archive_scene_node_t
{
    const char* id;
    const char* dlist_name;
    float matrix[16];
    gfx_archive_scene_node_vec_t child_nodes;
} gfx_archive_scene_node_t;

// Graphics archive - contains the vertices, dlist objects, images

typedef enum
{
    GAR_IMG_RGBA5551
} gfx_archive_img_type_t;

typedef struct
{
    const char* name;
    gfx_archive_img_type_t type;
    union
    {
        img_rgba5551_t img_rgba5551;
    } gimg;
} gfx_archive_img_t;

VECTOR_TYPEDEF(gfx_archive_img_t, gfx_archive_img_vec_t, gar_img_vec_)

typedef struct
{
    ucode_t ucode;
    uint32_t vbase;
    double vscale;
    dlist_object_vec_t dlist_objects;
    gfx_archive_img_vec_t images;
    gfx_archive_scene_node_t root_node;
} gfx_archive_t; // collection of dlists, vertices, images

// Vertex buffer & triangle command mapping context

typedef struct
{
    int v1, v2, v3;
} tri_t;

VECTOR_TYPEDEF(tri_t, tri_vector_t, tri_vector_)

typedef struct
{
    int max_vertices;
    int vidx;
    vertex_t vbuf[32];
    tri_vector_t tris;
} gbuf_t;

/////////////////////////////////////

int gar_init(gfx_archive_t* gar, ucode_t ucode, uint32_t vbase, double vscale);
int gar_scene_node_init(gfx_archive_scene_node_t* node, const char* id, const char* dlist_name);
int gar_scene_node_free(gfx_archive_scene_node_t* node);

gfx_archive_img_t* gar_get_img(gfx_archive_t* gar, const char* img_name);

// int gar_free(gfx_archive* gar);
int dlist_object_init(dlist_object_t* dlist_object, const char* name, ucode_t ucode);
int dlist_object_free(dlist_object_t* dlist_object);

// vbuf/triangle context
int gbuf_init(gbuf_t* gbuf, int max_vertices);
int gbuf_free(gbuf_t* gbuf);
int gbuf_clear(gbuf_t* gbuf);
int gbuf_is_empty(gbuf_t* gbuf);
int gbuf_get_vertex_index(gbuf_t* gbuf, vertex_t* vertex);
int gbuf_has_vertex(gbuf_t* gbuf, vertex_t* vertex);
int gbuf_push_unique_vertex(gbuf_t* gbuf, vertex_t vertex);
int gbuf_push_tri(gbuf_t* gbuf, int v1, int v2, int v3);
void gbuf_flush(gbuf_t* gbuf, dlist_object_t* dlist_object, uint32_t vertex_base_address);
int gbuf_has_room3(gbuf_t* gbuf, vertex_t* v0, vertex_t* v1, vertex_t* v2);

#endif // GFX_ARCHIVE_H