#include "vecprim.h"

VECTOR_IMPLDEF(float, float_vec_t, float_vec_)
VECTOR_IMPLDEF(double, double_vec_t, double_vec_)
VECTOR_IMPLDEF(int, int_vec_t, int_vec_)
VECTOR_IMPLDEF(const char*, cstr_vec_t, cstr_vec_)