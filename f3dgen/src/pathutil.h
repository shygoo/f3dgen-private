#ifndef PATHUTIL_H
#define PATHUTIL_H

void path_containing_dir(char* newpath, const char* path);
int removedir(const char* path);
int makedir(const char* path);

#endif