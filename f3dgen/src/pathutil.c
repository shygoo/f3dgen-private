#include <string.h>

#include "pathutil.h"

#ifdef _WIN32
    #define WIN32_LEAN_AND_MEAN 
    #include <windows.h>
#else
    #include <sys/stat.h>
    #include <unistd.h>
#endif

void path_containing_dir(char* newpath, const char* path)
{
    int len = strlen(path);
    strcpy(newpath, path);
    
    for(int i = len-1; i >= 0; i--)
    {
        if(newpath[i] == '/' || newpath[i] == '\\')
        {
            newpath[i + 1] = '\0';
            return;
        }
    }

    strcpy(newpath, ".");
}


int makedir(const char* path)
{
    #ifdef _WIN32
        return (CreateDirectory(path, NULL) == 0);
    #else
        return mkdir(path, 0700);
    #endif
}

int removedir(const char* path)
{
    #ifdef _WIN32
        return (RemoveDirectory(path) == 0);
    #else
        return rmdir(path);
    #endif
}