#ifndef DLIST_H
#define DLIST_H

#include "vecdef.h"

#define G_IM_FMT_RGBA  0
#define G_IM_FMT_YUV   1
#define G_IM_FMT_CI    2
#define G_IM_FMT_IA    3
#define G_IM_FMT_I     4

#define G_IM_SIZ_4b    0
#define G_IM_SIZ_8b    1
#define G_IM_SIZ_16b   2
#define G_IM_SIZ_32b   3

#define G_TX_NOMIRROR  0
#define G_TX_MIRROR    1
#define G_TX_WRAP      0
#define G_TX_CLAMP     2

// geo mode

#define G_ZBUFFER            0x00000001
#define G_SHADE              0x00000004
#define G_CULL_FRONT         0x00000200
#define G_CULL_BACK          0x00000400
#define G_FOG                0x00010000
#define G_LIGHTING           0x00020000
#define G_TEXTURE_GEN        0x00040000
#define G_TEXTURE_GEN_LINEAR 0x00080000
#define G_SHADING_SMOOTH     0x00200000
#define G_CLIPPING           0x00800000
#define G_ALL                0x00FFFFFF
#define G_NONE               0x00000000

// g_texture 0xd7
#define TEX_ON  2
#define TEX_OFF 0

#define TEX_SCALE_FULL 0xFFFF

// tile descriptors
#define TILE_0 0
#define TILE_1 1
#define TILE_2 2
#define TILE_3 3
#define TILE_4 4
#define TILE_5 5
#define TILE_6 6
#define TILE_7 7

// othermode_h

// shifts
#define G_MDSFT_BLENDMASK  0
#define G_MDSFT_ALPHADITHER  4
#define G_MDSFT_RGBDITHER  6
#define G_MDSFT_COMBKEY  8
#define G_MDSFT_TEXTCONV  9
#define G_MDSFT_TEXTFILT  12
#define G_MDSFT_TEXTLUT  14
#define G_MDSFT_TEXTLOD  16
#define G_MDSFT_TEXTDETAIL  17
#define G_MDSFT_TEXTPERSP  19
#define G_MDSFT_CYCLETYPE  20
#define G_MDSFT_COLORDITHER  22
#define G_MDSFT_PIPELINE  23

// field values
#define G_AD_PATTERN 0b00
#define G_AD_NOTPATTERN 0b01
#define G_AD_NOISE 0b10
#define G_AD_DISABLE 0b11

#define G_TT_NONE 0b00
#define G_TT_RGBA16 0b10
#define G_TT_IA16 0b11

#define G_TF_POINT   0b00
#define G_TF_BILERP  0b10 
#define G_TF_AVERAGE 0b11

typedef enum
{
	UCODE_F3DEX2
} ucode_t;

typedef struct
{
	uint32_t a;
	uint32_t b;
} dlist_command_t;

int dlist_command_swap(dlist_command_t* command);

VECTOR_TYPEDEF(dlist_command_t, dlist_command_vec_t, dlist_command_vec_)

typedef struct 
{
	ucode_t ucode;
	dlist_command_vec_t commands;
} dlist_t;

void dlist_init(dlist_t* dlist, ucode_t ucode);
void dlist_free(dlist_t* dlist);
int  dlist_push_command(dlist_t* dlist, dlist_command_t command);
void dlist_print(dlist_t* dlist);
int  dlist_size_bytes(dlist_t* dlist);
void dlist_copy(dlist_t* dlist, void* dest);

void dlist_bswap(dlist_t* dlist);
void dlist_dump(dlist_t* dlist, const char* path);

int dlist_push_tri1(dlist_t* dlist, uint8_t v0, uint8_t v1, uint8_t v2);
int dlist_push_tri2(dlist_t* dlist, uint8_t v0, uint8_t v1, uint8_t v2, uint8_t v10, uint8_t v11, uint8_t v12);
int dlist_push_vtx(dlist_t* dlist, uint32_t vaddr, uint8_t numv, uint8_t vidx);
int dlist_push_enddl(dlist_t* dlist);
int dlist_push_loadsync(dlist_t* dlist);
int dlist_push_pipesync(dlist_t* dlist);
int dlist_push_tilesync(dlist_t* dlist);
int dlist_push_settile(dlist_t* dlist, int fmt, int siz, int line, int tmem, int tile,
	int palette, int cmT, int maskT, int shiftT, int cmS, int maskS, int shiftS);
int dlist_push_settilesize(dlist_t* dlist, int tile, int uls, int ult, int lrs, int lrt);
int dlist_push_geometrymode(dlist_t* dlist, uint32_t clearbits, uint32_t setbits);

int dlist_push_settimg(dlist_t* dlist, int fmt, int siz, int width, uint32_t imgaddr);
int dlist_push_setcombine_raw(dlist_t* dlist, uint32_t hi, uint32_t lo);

int dlist_push_texture(dlist_t* dlist, int scaleS, int scaleT, int level, int tile, int on);

int dlist_push_loadblock(dlist_t* dlist, int tile, int uls, int ult, int texels, int dxt);

int dlist_push_othermode_h(dlist_t* dlist, int shift, int length, uint32_t data);

int dlist_push_setfillcolor(dlist_t* dlist, uint32_t color);
int dlist_push_setfogcolor(dlist_t* dlist, uint32_t color);
int dlist_push_setblendcolor(dlist_t* dlist, uint32_t color);
int dlist_push_setprimcolor(dlist_t* dlist, uint32_t color);
int dlist_push_setenvcolor(dlist_t* dlist, uint32_t color);

#endif // DLIST_H
