#ifndef GFX_ARCHIVE_DAE_H
#define GFX_ARCHIVE_DAE_H

#include "gfx_archive.h"

int gar_import_dae(gfx_archive_t* gar, dae_t* dae);
int gar_add_dae_geometry(gfx_archive_t* gar, dae_t* dae, dae_geometry_t* geometry, gbuf_t* gbuf);
int gar_add_dae_image(gfx_archive_t* gar, dae_image_t* dae_image);

int gar_add_dae_scene_node(gfx_archive_scene_node_t* gar_scene_parent_node, dae_scene_node_t* dae_scene_node, double scale);

#endif // GFX_ARCHIVE_DAE_H