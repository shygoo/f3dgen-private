#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "vertex.h"
#include "dlist.h"
#include "cmdline.h"
#include "gfx_archive.h"
#include "gfx_archive_dae.h"
#include "gfx_archive_obj.h"
#include "pathutil.h"

#include "bswap.h"

typedef struct
{
	const char* input_path;
	const char* root_path;
	const char* output_path;
	const char* vertex_output_path;
	uint32_t    vertex_base_address;
	double      vertex_scale;
	ucode_t     ucode;
} options_t;

int switch_o(cmd_args_t* args, void* p)
{
    options_t* options = (options_t*)p;

	const char* path = cmd_args_pop(args);

	if(path == NULL)
	{
        printf("-o requires path");
        return 0;
    }
    options->output_path = path;

	return 1;
}

int switch_ov(cmd_args_t* args, void* p)
{
	options_t* options = (options_t*)p;
	
	const char* path = cmd_args_pop(args);
	
	if(path == NULL)
	{
		printf("-ov requires path");
		return 0;
	}
	options->vertex_output_path = path;
	return 1;
}

int switch_ucode(cmd_args_t* args, void* p)
{
	options_t* options = (options_t*)p;
	
	const char* ucode_name = cmd_args_pop(args);
	
	if(ucode_name == NULL)
	{
		printf("-ucode requires ucode name\n");
		return 0;
	}
	
	if(strcmp(ucode_name, "F3DEX2") == 0)
	{
		options->ucode = UCODE_F3DEX2;
	}
	else
	{
		printf("invalid ucode\n");
		return 0;
	}
	
	//printf("vertex ouput path: %s\n", path);
	//options->vertex_output_path = path;
	return 1;
}

int switch_root(cmd_args_t* args, void* p)
{
	options_t* options = (options_t*)p;
	
	const char* root_path = cmd_args_pop(args);
	
	if(root_path == NULL)
	{
		printf("-root requires path\n");
		return 0;
	}
	
	options->root_path = root_path;
	return 1;
}

int switch_vbase(cmd_args_t* args, void* p)
{
	options_t* options = (options_t*)p;
	const char* vbase_str = cmd_args_pop(args);
	
	if(vbase_str == NULL)
	{
		printf("error: -vbase requires vertex base address\n");
		return 0;
	}
	
	uint32_t addr = strtoll(vbase_str, NULL, 0);
	
	options->vertex_base_address = addr;
	return 1;
}

int switch_vscale(cmd_args_t* args, void* p)
{
	options_t* options = (options_t*)p;
	const char* vscale_str = cmd_args_pop(args);
	
	if(vscale_str == NULL)
	{
		printf("error: -vscale requires decimal vertex scale\n");
		return 0;
	}
	
	double scale = strtod(vscale_str, NULL);
	options->vertex_scale = scale;
	return 1;
}

int arg_default(cmd_args_t* args, void* p)
{
	options_t* options = (options_t*)p;
	if(options->input_path != NULL)
	{
		printf("error: input path already set\n");
		return 0;
	}
	options->input_path = args->cur_arg;
	return 1;
}

void print_usage_guide()
{
	printf (
		"f3dgen - Fast3D generator                                                        \n"
		"                                                                                 \n"
		"f3dgen <input_path> [options]                                                    \n"
		"                                                                                 \n"
		"  -uc            <ucode_class>                                                   \n"
		"                  Sets the microcode class                                       \n"
		"                  Valid options: F3DEX2                                          \n"
		"                                                                                 \n"
		"  -o             <ucode_output_path>                                             \n"
		"                  Outputs Fast3D microcode data                                  \n"
		"                                                                                 \n"
		"  -ov            <vertices_output_path>                                          \n"
		"                  Outputs vertex data                                            \n"
		"                                                                                 \n"
		"  -o-vtx-rel     <vtx_relocations_output_path>                                   \n"
		"                  Outputs list of offsets to every G_VTX command in the ucode    \n"
		"                                                                                 \n"
		"  -o-settimg-rel <settimg_relocations_ouput_path>                                \n"
		"                  Outputs list of offsets to every G_SETTIMG command in the ucode\n"
		"                                                                                 \n"
		"  -vbase         <vertex_base_address>                                           \n"
		"                  Sets the base address that G_VTX will use for vertices         \n"
		"                                                                                 \n"
		"Input may be one of the following formats: Collada DAE (*.dae)\n"
	);
}

void gar_fp_print_scene_nodes(FILE* gar_fp, gfx_archive_scene_node_t* scene_node, int level)
{
	for(int i = 0; i < scene_node->child_nodes.length; i++)
	{
		gfx_archive_scene_node_t* child_node = &scene_node->child_nodes.array[i];
		
		for(int i = 0; i < level; i++)
			fprintf(gar_fp, "  ");
		fprintf(gar_fp, "node %s, %s, \"", child_node->id, child_node->dlist_name);

		for(int i = 0; i < 16; i++)
			fprintf(gar_fp, "%f ", child_node->matrix[i]);
		fprintf(gar_fp, "\"\n");

		gar_fp_print_scene_nodes(gar_fp, child_node, level + 1);
		
		for(int i = 0; i < level; i++)
			fprintf(gar_fp, "  ");

		fprintf(gar_fp, "endnode\n");
		//printf("%s\n", scene_node->id);
	}
}

int main(int argc, const char* argv[])
{
	options_t options;
	dae_t dae;
	gfx_archive_t gar;
	char path[256];
	char root_path[256];

	static cmd_switch_handler_t switches[] = {
		{ "o",      switch_o },
		{ "ov",     switch_ov },
		{ "uc",     switch_ucode },
		{ "root",   switch_root },
		{ "vbase",  switch_vbase },
		{ "vscale", switch_vscale },
		{ NULL, NULL }
    };
	
	if(argc < 2)
	{
		print_usage_guide();
		return EXIT_FAILURE;
	}
	
	options.root_path = NULL;
	options.input_path = NULL;
	options.output_path = NULL;
	options.vertex_output_path = NULL;
	options.vertex_base_address = 0;
	options.vertex_scale = 1.0f;
	options.ucode = UCODE_F3DEX2;
    
    if(!cmd_process(argc, argv, switches, arg_default, &options))
	{
		return EXIT_FAILURE;
	}

	if(options.input_path == NULL)
	{
		printf("error: no input path\n");
		return EXIT_FAILURE;
	}
	
	if(options.root_path == NULL)
	{
		path_containing_dir(root_path, options.input_path);
		options.root_path = root_path;
	}

	printf("[f3dgen] input_path:  %s\n", options.input_path);
	printf("[f3dgen] root_path:   %s\n", options.root_path);
	printf("[f3dgen] output_path: %s\n", options.output_path);
	printf("[f3dgen] vertex_base_address: %08X\n", options.vertex_base_address);
	printf("[f3dgen] vertex_scale: %f\n", options.vertex_scale);
	printf("[f3dgen] ucode: %d\n\n", options.ucode);

	if(!dae_load(&dae, options.input_path, options.root_path))
	{
		printf("an error occurred while loading the dae file\n");
		return EXIT_FAILURE;
	}

	gar_init(&gar, options.ucode, options.vertex_base_address, options.vertex_scale);
	gar_import_dae(&gar, &dae);

	// export graphics archive /////////
	
	printf("dumping gfx archive\n");

	removedir(options.output_path);
	makedir(options.output_path);

	

	sprintf(path, "%s/MANIFEST", options.output_path);
	FILE* gar_fp = fopen(path, "wb");

	// dump vertex heap

	printf("1 vertex heap created:\n");
	sprintf(path, "%s/vertices.bin", options.output_path);
	printf("%s\n", path);

	fprintf(gar_fp, "# F3DGEN GFX MANIFEST V0.1\n");
	
	const char* sz_ucode;

	switch(gar.ucode)
	{
	case UCODE_F3DEX2:
		sz_ucode = "F3DEX2";
		break;
	}

	fprintf(gar_fp, "# %s\n\n", sz_ucode);

	//fprintf(gar_fp, "vertices vertices, \"vertices.bin\"\n\n");
	//vertex_list_dump(&gar.vertices, path);
	
	// dump textures

	printf("%d texture image(s) created:\n", gar.images.length);

	for(int i = 0; i < gar.images.length; i++)
	{
		gfx_archive_img_t* gar_img = &gar.images.array[i];

		sprintf(path, "%s/%s.bin", options.output_path, gar_img->name);
		printf("  %s\n", path);

		fprintf(gar_fp, "image %s, \"%s.bin\"\n", gar_img->name, gar_img->name);

		switch(gar_img->type)
		{
		case GAR_IMG_RGBA5551:
			{
				img_rgba5551_t* img_rgba5551 = &gar_img->gimg.img_rgba5551;
				
				FILE* fp = fopen(path, "wb");
				
				bswap16_buffer(img_rgba5551->pixels, img_rgba5551->num_pixels * 2);
				fwrite(img_rgba5551->pixels, 1, img_rgba5551->num_pixels * 2, fp);
				bswap16_buffer(img_rgba5551->pixels, img_rgba5551->num_pixels * 2);

				fclose(fp);
			}
			break;
		}
	}

	fprintf(gar_fp, "\n");

	// dump dlist objects

	printf("%d dlist object(s) created:\n", gar.dlist_objects.length);

	for(int i = 0; i < gar.dlist_objects.length; i++)
	{
		dlist_object_t* dlist_object = &gar.dlist_objects.array[i];
		dlist_t* dlist = &dlist_object->dlist;
		dlist_rel_vec_t* relocations = &dlist_object->relocations;
		vertex_list_t* vlist = &dlist_object->vertices;

		char default_vertices_symbol[256];

		sprintf(default_vertices_symbol, "%s_vertices", dlist_object->name);

		fprintf(gar_fp, "verts %s, \"%s.bin\"\n", default_vertices_symbol, default_vertices_symbol);
		sprintf(path, "%s/%s_vertices.bin", options.output_path, dlist_object->name);
		vertex_list_dump(vlist, path);

		fprintf(gar_fp, "ucode %s, \"%s.bin\"\n", dlist_object->name, dlist_object->name);

		for(int j = 0; j < relocations->length; j++)
		{
			dlist_rel_t* rel = &relocations->array[j];
			
			fprintf(gar_fp, "  reloc ");

			const char* symbol;
			const char* sz_rel_type;

			switch(rel->type)
			{
				case DLIST_REL_VTX:
					sz_rel_type = "G_VTX";
					break;
				case DLIST_REL_SETTIMG:
					sz_rel_type = "G_SETTIMG";
					break;
				default:
					sz_rel_type = "DEFAULT";
					break;
			}

			if(rel->type == DLIST_REL_VTX && rel->symbol == NULL)
			{
				symbol = default_vertices_symbol;
			}
			else
			{
				symbol = rel->symbol;
			}

			fprintf(gar_fp, "0x%08X, %9s, %s", (rel->dlist_index * 8 + 4), sz_rel_type, symbol);

			if(rel->symbol_offset != 0)
			{
				fprintf(gar_fp, ", 0x%08X", rel->symbol_offset);
			}

			fprintf(gar_fp, "\n");
		}

		fprintf(gar_fp, "\n");

		sprintf(path, "%s/%s.bin", options.output_path, dlist_object->name);
		printf("  %s\n", path);
		dlist_dump(dlist, path);
	}

	// scene nodes

	gar_fp_print_scene_nodes(gar_fp, &gar.root_node, 0);
	
	fclose(gar_fp);

	/////////////////
}