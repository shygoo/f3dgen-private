package net.shygoo.f3dgen;

import javax.swing.*;
import javax.swing.filechooser.*;
import java.awt.event.*;

import java.io.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.*;

import java.util.ArrayList;


class DAEMaterial
{
    Node domNode;
    NamedNodeMap domNodeAttributes;

    public DAEMaterial(Node domNode)
    {
        this.domNode = domNode;
        domNodeAttributes = domNode.getAttributes();
    }

    public Node getNode()
    {
        return domNode;
    }

    public String getId()
    {
        Node materialNameNode = domNodeAttributes.getNamedItem("id");
        return materialNameNode.getTextContent();
    }

    public String getName()
    {
        Node materialIdNode = domNodeAttributes.getNamedItem("name");
        return materialIdNode.getTextContent();
    }

    public String getEffectId()
    {
        NodeList effectNodes = domNode.getChildNodes();

        Node effectNode = null;

        for(int i = 0; i < effectNodes.getLength(); i++)
        {
            Node childNode = effectNodes.item(i);
            String nodeName = childNode.getNodeName();

            if(nodeName.equals("instance_effect"))
            {
                effectNode = childNode;
                break;
            }
        }

        if(effectNode == null)
        {
            return null;
        }

        NamedNodeMap effectNodeAttributes = effectNode.getAttributes();
        Node effectNodeUrlNode = effectNodeAttributes.getNamedItem("url");

        // technically a url? could break?
        return effectNodeUrlNode.getTextContent().substring(1);
    }
}

class DAE
{
    Document doc;
    ArrayList<Node> materialNodes;
    ArrayList<Node> effectNodes;

    public DAE(File xmlFile) throws Exception
    {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        
        materialNodes = new ArrayList<Node>();

        doc = dBuilder.parse(xmlFile);
        doc.getDocumentElement().normalize();

        Node materialLibraryNode = doc.getElementsByTagName("library_materials").item(0);
        Node effectLibraryNode = doc.getElementsByTagName("library_effects").item(0);

        NodeList mtlLibChildNodes = materialLibraryNode.getChildNodes();
        //effectNodes = effectLibraryNode.getChildNodes();

        for(int i = 0; i < mtlLibChildNodes.getLength(); i++)
        {
            Node childNode = mtlLibChildNodes.item(i);

            if(childNode.getNodeName().equals("material"))
            {
                materialNodes.add(childNode);
            }
        }
    }

    int getNumMaterials()
    {
        return materialNodes.size();
    }

    DAEMaterial getMaterialByIndex(int index)
    {
        return new DAEMaterial(materialNodes.get(index));
    }
}

public class MainWindow extends JFrame
{
    private JMenuBar     comMenuBar;
    private JMenu        comFileMenu;
    private JMenuItem    comFileMenuOpen;
    private JFileChooser comFileChooser;

    private DAE dae;

    private void comFileMenuOpenClicked()
    {
        int fileChooserResult = comFileChooser.showOpenDialog(this);

        if(fileChooserResult != JFileChooser.APPROVE_OPTION)
        {
            return;
        }

        File xmlFile = comFileChooser.getSelectedFile();
        
        try
        {
            dae = new DAE(xmlFile);

            int numMaterials = dae.getNumMaterials();
            
            for(int i = 0; i < numMaterials; i++)
            {
                DAEMaterial material = dae.getMaterialByIndex(i);
                System.out.println(material.getId() + " " + material.getName() + " " + material.getEffectId());
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public MainWindow()
    {
        super("F3DGEN Material Editor");
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        // define components

        comMenuBar = new JMenuBar();
        comFileMenu = new JMenu("File");
        comFileMenuOpen = new JMenuItem("Open...");

        comFileChooser = new JFileChooser();
        comFileChooser.setFileFilter(new FileNameExtensionFilter("Collada DAE (*.dae)", "dae"));

        // add action listeners

        comFileMenuOpen.addActionListener((ActionEvent ev) -> {
            comFileMenuOpenClicked();
        });

        // create component tree

        comFileMenu.add(comFileMenuOpen);
        comMenuBar.add(comFileMenu);
        setJMenuBar(comMenuBar);

        setSize(400, 500);
        
        System.out.println("main window created");
    }
}