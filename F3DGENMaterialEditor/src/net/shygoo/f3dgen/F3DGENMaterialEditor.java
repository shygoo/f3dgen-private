package net.shygoo.f3dgen;

import javax.swing.UIManager;

import net.shygoo.f3dgen.MainWindow;

public class F3DGENMaterialEditor
{
    public static void main(String args[])
    {
        System.out.println("F3DGEN Material Editor");
    
        try
        {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (Exception e)
        {
            System.out.println("setLookAndFeel failed");
        }
        
        MainWindow mainWindow = new MainWindow();
        mainWindow.setVisible(true);
    }
}