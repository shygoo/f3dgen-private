#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

size_t fsize(FILE* fp)
{
    fseek(fp, 0, SEEK_END);
    size_t size = ftell(fp);
    rewind(fp);
    return size;
}

int main(int argc, const char* argv[])
{
    const char *target_file, *source_file, *output_file;
    FILE *target_fp, *source_fp, *output_fp;
    size_t source_size, target_size;
    uint32_t target_address;
    char *output_buffer;
    size_t num_bytes_read;

    if(argc < 5)
    {
        printf("insertbin\nusage: insertbin <target_address> <target_file> <source_file> <output_file>\n");
        return EXIT_FAILURE;
    }

    target_address = strtol(argv[1], NULL, 0);

    target_file = argv[2];
    source_file = argv[3];
    output_file = argv[4];

    if((target_fp = fopen(target_file, "rb")) == NULL)
    {
        printf("[insertbin] failed to open '%s'\n", target_file);
        return EXIT_FAILURE;
    }
    
    if((source_fp = fopen(source_file, "rb")) == NULL)
    {
        printf("[insertbin] failed to open '%s'\n", source_file);
        fclose(target_fp);
        return EXIT_FAILURE;
    }

    if((output_fp = fopen(output_file, "wb")) == NULL)
    {
        printf("[insertbin] failed to open '%s'\n", output_file);
        fclose(target_fp);
        fclose(source_fp);
        return EXIT_FAILURE;
    }

    source_size = fsize(source_fp);
    target_size = fsize(target_fp);

    if(target_address + source_size > target_size)
    {
        printf("[insertbin] source file too large\n");
        return EXIT_FAILURE;
    }

    output_buffer = malloc(target_size);

    num_bytes_read = fread(output_buffer, 1, target_size, target_fp);

    if(num_bytes_read != target_size)
    {
        goto error_cleanup;
    }

    num_bytes_read = fread(&output_buffer[target_address], 1, source_size, source_fp);

    if(num_bytes_read != source_size)
    {
        goto error_cleanup;
    }

    fwrite(output_buffer, 1, target_size, output_fp);

    free(output_buffer);
    fclose(target_fp);
    fclose(source_fp);
    fclose(output_fp);    
    return EXIT_SUCCESS;

    error_cleanup:
    free(output_buffer);
    fclose(target_fp);
    fclose(source_fp);
    fclose(output_fp);
    return EXIT_FAILURE;

}